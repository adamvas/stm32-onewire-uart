/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void tobits(char* input){
	
	char a;
	HAL_UART_Transmit(&huart2,"\n Binary responce:",sizeof("\n Binary responce:")-1,1230);
	for(int k = 0; k < 9 ;k++){
		for (int i=7;i>-1;i--){
			if(( input[k] & (1<<i)) != 0  ) a='1';
			else a = '0';
			HAL_UART_Transmit(&huart2,&a,1,1230);
		}
		HAL_UART_Transmit(&huart2," ",1,1230);

	}
}

float gettemp(char* input){
    uint8_t lsb = input[0];
    uint8_t msb = input[1];
	switch(input[4]){
		case(0x1F)://9bit res
				lsb &= 0xF8;
				break;
		case(0x3F)://10bit res
				lsb &= 0xFC;
				break;
		case(0x5F)://11bit res
				lsb &= 0xFE;
				break;
		case(0x7F)://12bit res
				break;
		default: return -9999;
	}
	uint8_t sign = msb & 0x80;
	int16_t temp = (msb << 8) + lsb;
	if (sign) {
		temp = ((temp ^ 0xffff) + 1) * -1;
	}
	return temp / 16.0;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */



  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
char buff[10] = {0,0,0,0,0,0,0,0,0,0};

void DS18B20_OnComplete(void){
	HAL_UART_Transmit(&huart2,"\n Complete",sizeof("\n Complete")-1,1230);
}
void DS18B20_Error(void){
	HAL_UART_Transmit(&huart2,"\n Error:",sizeof("\n Error")-1,1230);
}

  OneWire_SetCallback(DS18B20_OnComplete, DS18B20_Error);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  	for(int i=0; i<10;i++)buff[i]=0;
	  	OneWire_Init();
		OneWire_Execute(0xcc,0,0x00,0); /* skip rom phase */
		OneWire_Execute(0xcc,0,0x44,0); /* start to Convert T */
		HAL_Delay(1000); /* Wait to convertion time */
		OneWire_Execute(0xcc,0,0x00,0); /* skip rom phase */
		OneWire_Execute(0xcc,0,0xbe,buff); /* start to read configuration & result */
		HAL_Delay(500);



		tobits(buff);

		/* float printing process from here https://stackoverflow.com/questions/905928/using-floats-with-sprintf-in-embedded-c */
		float temperature = gettemp(buff);
		char str[100] = {0} ;
		char *tmpSign = (temperature < 0) ? "-" : "";
		float tmpVal = (temperature < 0) ? -temperature : temperature;
		int tmpInt1 = tmpVal;                  // Get the integer (678).
		float tmpFrac = tmpVal - tmpInt1;      // Get fraction (0.0123).
		int tmpInt2 = trunc(tmpFrac * 10000);  // Turn into integer (123).
		// Print as parts, note that you need 0-padding for fractional bit.
		sprintf (str, "\n Temperature = %s%d.%04d\n", tmpSign, tmpInt1, tmpInt2);
		HAL_UART_Transmit(&huart2,&str,sizeof(str)-1,1230);

		HAL_Delay(1000);
	  

		/*optional: set accuracy to 12 bit*/
		if(buff[4]!=0x7F){
			buff[4]=0x7F;
			OneWire_Execute(0xcc,0,0x4e,buff[2]);
			HAL_UART_Transmit(&huart2,"\n ********** \n changing accuracy to 12 bits \n ********** \n",sizeof("\n ********** \n changing accuracy to 12 bits\n ********** \n")-1,1230);
		}



    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSE|RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 16;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable MSI Auto calibration 
  */
  HAL_RCCEx_EnableMSIPLLMode();
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
